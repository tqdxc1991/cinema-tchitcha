@extends('layouts.app')

@section('content')
create a new movie
<form action="/movies" method="POST">
@csrf 
<label for="title" >title</label>
<input type="text" id="title" name="title">
<label for="released" >released</label>
<input type="date" id="released" name="released">
<label for="plot" >plot</label>
<input type="text" id="plot" name="plot">
<label for="runtime" >runtime</label>
<input type="number" id="runtime" name="runtime">
<label for="poster" >poster</label>
<input type="text" id="poster" name="poster">
<label for="imdbId" >imdbId</label>
<input type="number" id="imdbId" name="imdbId">

<input type="submit" value="create">
</form>
@endsection