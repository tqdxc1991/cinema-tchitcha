@extends('layouts.app')

@section('content')
   
<a href="/movies">Back to movies page</a>
<form action="/movies/{{ $movie ->id}}" method="POST">
@csrf
@method('DELETE')
<button>delete movie</button>
</form>
    <div class="card-deck">
        <div class="card">
            <img class="card-img-top" src="{{$movie->poster}}" alt="Card image cap">
            <div class="card-body">
            <h5 class="card-title">{{$movie->title}}</h5>
            <p class="card-text">{{$movie->plot}}</p>
            </div>
            <div class="card-footer">
            <small class="text-muted">{{$movie->runtime}} mins</small>
            </div>
        </div>
    </div>
    @endsection