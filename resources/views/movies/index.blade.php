@extends('layouts.app')

@section('content')
 <div class="container">
    <a href="/movies/create">Add a new movie</a>
        <p>{{ session('msg')}} </p>
        <div class="card-deck">
        @foreach ($movies as $movie)
            <div class="card">
                <img class="card-img-top" src="{{$movie->poster}}" alt="Card image cap">
                <div class="card-body">
                <h5 class="card-title">{{$movie->title}}</h5>
                <p class="card-text">{{$movie->plot}}</p>
                </div>
                <div class="card-footer">
                <small class="text-muted">{{$movie->runtime}} mins</small>
                </div>
            </div>
        @endforeach
        </div>
 </div>
    
    @endsection