<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Movie;

class MovieController extends Controller
{
    
    public function index()
    {
        return view('movies.index', [
            'movies' => Movie::All() 
        ]);
    }

    public function show($id)
    {
        return view('movies.show', [
            'movie' => Movie::FindOrFail($id) 
        ]); 
    }

    public function create(){
        return view('movies.create');
    }

    public function store(){
        $movie = new Movie();

        $movie->title = (request('title'));
        $movie->released = (request('released'));
        $movie->plot = (request('plot'));
        $movie->runtime = (request('runtime'));
        $movie->poster = (request('poster'));
        $movie->imdbId = (request('imdbId'));
        $movie->status = 1;
        $movie->title = (request('title'));
       error_log($movie);
       $movie->save();
        return redirect('/movies')->with('msg','movie addes successfully!');
    }

    public function destroy($id)
    {
      $movie = Movie::FindOrFail($id);
      $movie->delete();
      return redirect('/movies') ;
    }
}
